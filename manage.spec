# -*- mode: python -*-

block_cipher = None


a = Analysis(['manage.py'],
             pathex=['/Users/ccty/Projects/lottery'],
             binaries=[],
             datas=[
             ('/Users/ccty/Projects/lottery/lottery/static_root','./lottery/static_root'), 
             ('/Users/ccty/Projects/lottery/lottery/templates','./lottery/templates'),
             ('/miniconda3/envs/py3/lib/python3.7/site-packages/import_export/templates','./lottery/templates'),
             ('/miniconda3/envs/py3/lib/python3.7/site-packages/import_export/templatetags','./django/templatetags'),

             ],
             hiddenimports=[
                 'lottery',
                 'import_export',
                 'lottery.apps',
                 'lottery.urls',
                 'lottery.models',
                 'lottery.admin',
                
                'django.contrib.admin.apps',
                'django.contrib.auth.apps',
                'django.contrib.contenttypes.apps',
                'django.contrib.sessions.apps',
                'django.contrib.messages.apps',
                'django.contrib.staticfiles',],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='manage',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='manage')
